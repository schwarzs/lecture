#ifndef MAX_HPP
#define MAX_HPP

namespace myops{

  template <class T>
  T max(T x, T y)
  {
  	return (x > y) ? x : y;
  }


}

#endif // MAX_HPP
